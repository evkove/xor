README.txt

To complete this task you should solve a task and pass all tests.
Task should be made in your own repo, which must be forked from general:
https://gitlab.com/esid-learn/c/

Here is gitlab repo fork help:
https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork

All your tasks for all labs will should be done in this repo.

0. Sign in gitlab with your own credentials

1. Goto https://gitlab.com/esid-learn/c/

2. Press Fork button on upper right corner

3. Select namespace to clone (you must be logged in with your gitlab account)

4. After that you should get something like:
   https://gitlab.com/your_login/c

Go to your PC's console and clone newly forked repo:

git clone https://gitlab.com/your_login/c.git

5. Make couple or C programming to solve a task

6. Commit your changes

git commit -m "Solve task ...."

7. Push your changed to your branch

git push -u origin master

9. Go to https://gitlab.com/your_login/c webpage and check CICD/Pipelines page

10. Your commit is to be build and tested

Wait until all task would be completed.
At each stage of test you can see problems you have by clicking on stage icon with the mouse button.

 [V][V][X]

You can solve issues with additional commits to pass tests

11. When you reach 3 green check marks, your task is fully complete

 [V][V][V]

12. Add @vslapik to reviewers to get task fully complete

    Project's Settings/Members/InviteMember @vslapik as Maintainer



